import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class ConnexionBDD {

    private Connection myConnexion;
    private String userbdd;
    private String passwordbdd;
    private String Urlbdd;
    private String cheminFichierProperties;
    private PreparedStatement ps;

    public ConnexionBDD(String cheminFichierProperties) throws IOException, SQLException {
        this.cheminFichierProperties = cheminFichierProperties;
    }

    public void seConnecter() throws SQLException, IOException {
        Properties props = new Properties();
        props.load(new FileInputStream(cheminFichierProperties));
        userbdd = props.getProperty("user");
        passwordbdd = props.getProperty("password");
        Urlbdd = props.getProperty("dburl");
        myConnexion = DriverManager.getConnection(Urlbdd, userbdd, passwordbdd);
    }

    public Connection getMaConnexion() {
        return myConnexion;
    }

    public void insererDonnees(String prenom, String nom, String dateNaissance) throws SQLException {
        String query = "Insert into person (first_name,last_name,dob) values (?,?,?)";
        ps = myConnexion.prepareStatement(query);

        ps.setString(1,prenom);
        ps.setString(2,nom);
        ps.setString(3,dateNaissance);
        ps.executeUpdate();
    }

    public void showName() {
        String query = "SELECT first_name FROM person";


        try {
            Statement st = myConnexion.createStatement();
            ResultSet rs = st.executeQuery(query);

            while (rs.next()) {
                System.out.format("Prenom : %s\n", rs.getString("first_name"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void updateValues(Integer id, String prenom, String nom, String dateNaissance) throws SQLException {
        String query = "UPDATE person SET first_name = ?, last_name = ?, dob = ? WHERE id = ?";

        ps = myConnexion.prepareStatement(query);

        ps.setString(1,prenom);
        ps.setString(2,nom);
        ps.setString(3,dateNaissance);
        ps.setInt(4,id);
        ps.executeUpdate();
    }

    public void eraseAll() throws SQLException {
        String query = "DELETE FROM person";
        Statement st = myConnexion.createStatement();
        st.executeUpdate(query);
    }

}
