import java.io.IOException;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args) throws SQLException {


        try {
            ConnexionBDD maCo = new ConnexionBDD("C:\\Users\\guill\\IdeaProjects\\projet-cesi\\sql\\db.properties");
            maCo.seConnecter();
            maCo.insererDonnees("guillaume","mbali","19980410");
            maCo.insererDonnees("pierre","dupond", "19970210");
            maCo.showName();
            System.out.println("------------------------------------------");
            maCo.updateValues(1,"dimitrie","janot","20050402");
            maCo.showName();
            System.out.println("------------------------------------------");
            maCo.eraseAll();
            maCo.showName();
            System.out.println("------------------------------------------");


        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}